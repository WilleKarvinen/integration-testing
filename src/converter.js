const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex)
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16)
        const greenHex = green.toString(16)
        const blueHex = blue.toString(16)

        return pad(redHex) + pad(greenHex) + pad(blueHex) // "ff0000"
    },
    hexToRGB: (hex) => {
        // Split from every second character
        let hexArr = hex.match(/.{1,2}/g)

        // Convert from hex to decimal
        const redDec = parseInt(hexArr[0], 16)
        const greenDec = parseInt(hexArr[1], 16)
        const blueDec = parseInt(hexArr[2], 16)

        return `${redDec}, ${greenDec}, ${blueDec}` // "255, 0, 0"
    }
}